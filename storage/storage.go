package storage

import (
	"catalog/genproto/catalog_service"
	"context"
)

type StoregeI interface {
	Product() ProductI
	Category() CategoryI
}

type ProductI interface {
	Create(context.Context, *catalog_service.CreateProduct) (string, error)
	Update(context.Context, *catalog_service.Product) (string, error)
	Get(context.Context, *catalog_service.IdReqRes) (*catalog_service.Product, error)
	GetAll(context.Context, *catalog_service.GetAllProductRequest) (*catalog_service.GetAllProductResponse, error)
	Delete(context.Context, *catalog_service.IdReqRes) (string, error)
	GetByBarcode(context.Context, *catalog_service.GetByBarcodereq) (*catalog_service.Product, error)
}

type CategoryI interface {
	Create(context.Context, *catalog_service.CreateCategory) (string, error)
	Update(context.Context, *catalog_service.Category) (string, error)
	Get(context.Context, *catalog_service.IdReqRes) (*catalog_service.Category, error)
	GetAll(context.Context, *catalog_service.GetAllCategoryRequest) (*catalog_service.GetAllCategoryResponse, error)
	Delete(context.Context, *catalog_service.IdReqRes) (string, error)
}
