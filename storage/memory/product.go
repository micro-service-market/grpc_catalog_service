package memory

import (
	"catalog/genproto/catalog_service"
	"catalog/packages/helper"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type productRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) *productRepo {

	return &productRepo{
		db: db,
	}

}

func (t *productRepo) Create(ctx context.Context, req *catalog_service.CreateProduct) (string, error) {

	id := uuid.NewString()

	query := `
	INSERT INTO
		products(id,category_id,name,barcode,price)
	VALUES($1,$2,$3,$4,$5)`

	_, err := t.db.Exec(ctx, query,
		id,
		req.CategoryId,
		req.Name,
		req.Barcode,
		req.Price,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil

}

func (t *productRepo) Update(ctx context.Context, req *catalog_service.Product) (string, error) {

	query := `
	UPDATE
		products
	SET
		category_id=$2,name=$3,barcode=$4,price=$5,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := t.db.Exec(ctx, query,
		req.Id,
		req.CategoryId,
		req.Name,
		req.Barcode,
		req.Price,
	)
	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}
	return "OK", nil
}

func (t *productRepo) Get(ctx context.Context, req *catalog_service.IdReqRes) (*catalog_service.Product, error) {

	query := `SELECT
	id,
	category_id,
	name,
	barcode,
	price,
	created_at::text,
	updated_at::text
	FROM products WHERE id=$1`

	resp := t.db.QueryRow(ctx, query, req.Id)

	var product catalog_service.Product

	err := resp.Scan(
		&product.Id,
		&product.CategoryId,
		&product.Name,
		&product.Barcode,
		&product.Price,
		&product.CreatedAt,
		&product.UpdatedAt,
	)

	if err != nil {
		fmt.Println("Error from Select")
		return &catalog_service.Product{}, err
	}

	return &product, nil
}

func (t *productRepo) Delete(ctx context.Context, req *catalog_service.IdReqRes) (string, error) {

	query := `DELETE FROM products WHERE id = $1`

	resp, err := t.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (t *productRepo) GetAll(ctx context.Context, req *catalog_service.GetAllProductRequest) (resp *catalog_service.GetAllProductResponse, err error) {

	var (
		params  = make(map[string]interface{})
		filter  = " WHERE true"
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `SELECT
	id,
	category_id,
	name,
	barcode,
	price,
	created_at::text,
	updated_at::text
	FROM products WHERE id=$1`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		products
	`

	// if req.TransactionType != "" {
	// 	filter += ` AND transaction_type ILIKE '%' || @transaction_type || '%' `
	// 	params["transaction_type"] = req.TransactionType
	// }

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	q, pArr := helper.ReplaceQueryParams(query, params)
	rows, err := t.db.Query(ctx, q, pArr...)
	if err != nil {
		return &catalog_service.GetAllProductResponse{}, err
	}

	err = t.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &catalog_service.GetAllProductResponse{}, err
	}

	defer rows.Close()

	result := []*catalog_service.Product{}

	for rows.Next() {

		var product catalog_service.Product

		err := rows.Scan(
			&product.Id,
			&product.CategoryId,
			&product.Name,
			&product.Barcode,
			&product.Price,
			&product.CreatedAt,
			&product.UpdatedAt,
		)
		if err != nil {
			return &catalog_service.GetAllProductResponse{}, err
		}

		result = append(result, &product)

	}

	return &catalog_service.GetAllProductResponse{Products: result, Count: int64(count)}, nil
}

func (t *productRepo) GetByBarcode(ctx context.Context, req *catalog_service.GetByBarcodereq) (*catalog_service.Product, error) {

	query := `SELECT
	id,
	category_id,
	name,
	barcode,
	price,
	created_at::text,
	updated_at::text
	FROM products WHERE barcode=$1`

	resp := t.db.QueryRow(ctx, query, req.Barcode)

	var product catalog_service.Product

	err := resp.Scan(
		&product.Id,
		&product.CategoryId,
		&product.Name,
		&product.Barcode,
		&product.Price,
		&product.CreatedAt,
		&product.UpdatedAt,
	)

	if err != nil {
		fmt.Println("Error from Select")
		return &catalog_service.Product{}, err
	}

	return &product, nil
}
