package memory

import (
	"catalog/genproto/catalog_service"
	"catalog/packages/helper"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type categoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) *categoryRepo {

	return &categoryRepo{
		db: db,
	}

}

func (s *categoryRepo) Create(ctx context.Context, req *catalog_service.CreateCategory) (string, error) {

	var id = uuid.NewString()

	query := `
	INSERT INTO
	categories (id,parent_id,name)
	VALUES ($1,$2,$3)`

	_, err := s.db.Exec(ctx, query,
		id,
		req.ParentId,
		req.Name,
	)
	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil
}

func (s *categoryRepo) Update(ctx context.Context, req *catalog_service.Category) (string, error) {

	query := `
	UPDATE
	categories
	SET
		parent_id=$2,name=$3,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := s.db.Exec(ctx, query,
		req.Id,
		req.ParentId,
		req.Name,
	)
	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}
	return "OK", nil
}

func (s *categoryRepo) Get(ctx context.Context, req *catalog_service.IdReqRes) (*catalog_service.Category, error) {

	query := `
	SELECT
	id,
	parent_id,
	name,
	created_at::text,
	updated_at::text
	FROM categories
	WHERE id=$1`

	resp := s.db.QueryRow(ctx, query, req.Id)

	var category catalog_service.Category

	err := resp.Scan(
		&category.Id,
		&category.ParentId,
		&category.Name,
		&category.CreatedAt,
		&category.UpdatedAt,
	)

	if err != nil {
		return &catalog_service.Category{}, err
	}

	return &category, nil
}

func (s *categoryRepo) Delete(ctx context.Context, req *catalog_service.IdReqRes) (string, error) {

	query := `DELETE FROM categories WHERE id = $1`

	resp, err := s.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (s *categoryRepo) GetAll(ctx context.Context, req *catalog_service.GetAllCategoryRequest) (resp *catalog_service.GetAllCategoryResponse, err error) {

	var (
		params  = make(map[string]interface{})
		filter  = " WHERE true"
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `
	SELECT
	id,
	parent_id,
	name,
	created_at::text,
	updated_at::text
	FROM categories
	WHERE id=$1`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		categories
	`

	if req.Name != "" {
		filter += ` AND name ILIKE '%' || @name || '%' `
		params["name"] = req.Name
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	q, pArr := helper.ReplaceQueryParams(query, params)
	rows, err := s.db.Query(ctx, q, pArr...)
	if err != nil {
		return &catalog_service.GetAllCategoryResponse{}, err
	}

	err = s.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &catalog_service.GetAllCategoryResponse{}, err
	}

	defer rows.Close()

	result := []*catalog_service.Category{}

	for rows.Next() {

		var category catalog_service.Category

		err := rows.Scan(
			&category.Id,
			&category.ParentId,
			&category.Name,
			&category.CreatedAt,
			&category.UpdatedAt,
		)
		if err != nil {
			return &catalog_service.GetAllCategoryResponse{}, err
		}

		result = append(result, &category)

	}

	return &catalog_service.GetAllCategoryResponse{Categories: result, Count: int64(count)}, nil

}
