package service

import (
	"catalog/genproto/catalog_service"
	grpc_client "catalog/grpc/client"
	"catalog/packages/logger"
	"catalog/storage"
	"context"
)

type CategoryService struct {
	logger  logger.LoggerI
	storage storage.StoregeI
	clients grpc_client.GrpcClientI
	catalog_service.UnimplementedCategoryServiceServer
}

func NewCategoryService(log logger.LoggerI, strg storage.StoregeI, grpcClients grpc_client.GrpcClientI) *CategoryService {
	return &CategoryService{
		logger:  log,
		storage: strg,
		clients: grpcClients,
	}
}

func (t *CategoryService) Create(ctx context.Context, req *catalog_service.CreateCategory) (*catalog_service.IdReqRes, error) {
	id, err := t.storage.Category().Create(ctx, req)
	if err != nil {
		return nil, err
	}

	return &catalog_service.IdReqRes{Id: id}, nil
}

func (t *CategoryService) Update(ctx context.Context, req *catalog_service.Category) (*catalog_service.ResponseString, error) {
	str, err := t.storage.Category().Update(ctx, req)
	if err != nil {
		return nil, err
	}

	return &catalog_service.ResponseString{Text: str}, nil
}

func (t *CategoryService) Get(ctx context.Context, req *catalog_service.IdReqRes) (*catalog_service.Category, error) {
	category, err := t.storage.Category().Get(ctx, req)
	if err != nil {
		return nil, err
	}

	return category, nil
}

func (t *CategoryService) GetAll(ctx context.Context, req *catalog_service.GetAllCategoryRequest) (*catalog_service.GetAllCategoryResponse, error) {
	categories, err := t.storage.Category().GetAll(ctx, req)
	if err != nil {
		return nil, err
	}

	return categories, nil
}

func (t *CategoryService) Delete(ctx context.Context, req *catalog_service.IdReqRes) (*catalog_service.ResponseString, error) {
	text, err := t.storage.Category().Delete(ctx, req)
	if err != nil {
		return nil, err
	}

	return &catalog_service.ResponseString{Text: text}, nil
}
