package grpc

import (
	"catalog/genproto/catalog_service"
	grpc_client "catalog/grpc/client"
	"catalog/grpc/service"
	"catalog/packages/logger"
	"catalog/storage"

	"google.golang.org/grpc"
)

func SetUpServer(log logger.LoggerI, strg storage.StoregeI, grpcClient grpc_client.GrpcClientI) *grpc.Server {
	s := grpc.NewServer()
	catalog_service.RegisterCategoryServiceServer(s, service.NewCategoryService(log, strg, grpcClient))
	catalog_service.RegisterProductServiceServer(s, service.NewProductService(log, strg, grpcClient))
	return s
}
